# Oppsummering fra sist
sikkerhetsklarert = False
while True:
    navn = input("Skriv inn det hemmelige passordet: ")
    if navn == 'Børge':
        print('Børge? Du slipper aaaaaldri over denne broen!')
        break
    if navn == 'Rappo':
        if input('Rappo hvem?') == 'reven Rappo':
            sikkerhetsklarert = True
            break

print(f'Ble du sikkerhetsklarert? {sikkerhetsklarert}')

# Funksjoner

    # Første linje: 'funksjonshode' - husk parenteser og kolon!
def funksjonsnavn(innparameter1, innparameter2): # 1 og 41 blir her parametre
    # kodeblokk med ting en må gjøre for å komme til svaret
    svar = innparameter1 + innparameter2 # Bare et eksempel, altså
    return svar # valgfritt om en returnerer noe spesifikt

print(funksjonsnavn(1, 3))

# Et kjapt eksempel:
def gange(tall1, tall2):
    return tall1 * tall2

print(f'2*3 = {gange(2,3)}')

# Dagens plan:

# Flere funksjoner:
# Sjekk ut flere_funksjoner.py


# skop: sjekk ut skop.py
# - legg merke til at bruken av samme variabelnavn
# - legg merke til at funksjonen kjenner til navn
# - legg merke til at kode utenfor en funksjon ikke kjenner til en inni!
# Vis ppt-siden med skoping, Terje side 4 fra uke 39 

# Oppgave:
# Åpne oppgave_funksjoner.py


# Globale variable:
# Sjekk ut globale_variable.py
# ppt Terje s6 uke 39

# Biblioteker
# Se matte_bibliotek.py
# Se random_stuff.py

# Oppgave:
# Se oppgave_funksjoner.py

# Hva er en modul _egentlig_ - jo, bare en fil!
# Se min_modul.py
import min_modul
min_modul.min_funksjon("hei på deg")

# Et stort eksempel: romertall_3.py