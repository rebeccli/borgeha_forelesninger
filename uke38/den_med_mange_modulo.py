
# Problemet mitt var at jeg rotet det til med når
# jeg skulle skrive ut og ikke. Det var printene som rotet med
# hodet mitt.

# Målet her: skrive ut alle tall som er delelige på 13, 7 OG 3
# men: hvis tallet ikke er delelig på 13, da gidder vi ikke teste
# om det er delelig på 7 etterpå, men vi ønsker å bare hoppe til toppen
# av løkken igjen, med tall 1 høyere enn sist. DETTE gjør continue.
for i in range(1,1000):
    if i % 13 != 0: # hvis tallet ikke akkurat går opp i 13
        continue    # hopp ut av løkken. Alle tall som går videre: 13, 26 osv
    if i % 7 != 0:  # hvis tallet ikke akkurat går opp i 7
        continue
    if i % 3 != 0:
        continue
    print(i) # her ender vi opp hvis alle testene er true. Går opp i alle tall!
    
        
